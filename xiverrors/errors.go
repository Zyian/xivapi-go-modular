package xiverrors

import "errors"

var (
	ErrClientKeyMismatch = errors.New("Default client specified but no key given")
)

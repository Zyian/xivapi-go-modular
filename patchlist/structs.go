package patchlist

import (
	"encoding/json"
	"time"

	"gitlab.com/Zyian/xivapi-go-modular/client"
)

// PatchEntries is a simple typed array
type PatchEntries []*PatchEntry

// PatchEntry is a single PatchEntry of the PatchEntries list
type PatchEntry struct {
	TranslateableName

	Banner          string
	ExVersion       int
	ID              int
	IsExpansionRaw  int    `json:"IsExpansion"`
	ReleaseDateRaw  string `json:"ReleaseDate"`
	ReleaseDateTime time.Time
	Version         json.Number
}

// TranslateableName holds all different names for an object
// Only used on search so far, but used in different structs
type TranslateableName struct {
	Name   string `json:"Name"`
	NameCN string `json:"Name_cn"`
	NameDE string `json:"Name_de"`
	NameEN string `json:"Name_en"`
	NameFR string `json:"Name_fr"`
	NameJA string `json:"Name_ja"`
	NameKR string `json:"Name_kr"`
}

// PatchListClient is a wrapper struct for PatchList functions using a singleton http client
type PatchListClient struct {
	rootClient *client.XIVAPIWeb
}

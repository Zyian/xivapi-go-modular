package patchlist

import (
	"fmt"
	"net/url"
	"strconv"
	"time"

	"gitlab.com/Zyian/xivapi-go-modular/client"
	"gitlab.com/Zyian/xivapi-go-modular/xiverrors"
)

func NewPatchList(defaultClient bool, key string) (*PatchListClient, error) {
	if defaultClient && key == "" {
		return nil, xiverrors.ErrClientKeyMismatch
	}

	c := new(PatchListClient)
	if defaultClient {
		c.AttachWebClient(client.New(key))
	}

	return c, nil
}

func (c *PatchListClient) AttachWebClient(root *client.XIVAPIWeb) {
	c.rootClient = root
}

func (p PatchEntries) parseTime() {
	for _, e := range p {
		i, _ := strconv.ParseInt(e.ReleaseDateRaw, 10, 0)
		e.ReleaseDateTime = time.Unix(i, 0)
	}
}

// IsExpansion converts the IsExpansionRaw integer into a bool.
// This call treats -1 (no info available and not from this series) as false too
func (e PatchEntry) IsExpansion() bool {
	return e.IsExpansionRaw > 0
}

func (e PatchEntry) String() string {
	return fmt.Sprintf("Patch{ %v (%v) [%v] }", e.Name, e.Version, e.ReleaseDateTime)
}

// PatchList gets all known patches from the API
func (c *PatchListClient) PatchList() (PatchEntries, error) {
	uri, _ := url.Parse(fmt.Sprintf("%v%v", client.BaseURL, "/PatchList"))
	r := make(PatchEntries, 0, 25)

	if err := c.rootClient.RequestJSON(client.MethodGet, uri, nil, &r); err != nil {
		return nil, err
	}

	r.parseTime()

	return r, nil
}

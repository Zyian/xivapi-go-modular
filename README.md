# XIVAPI-Go Modular Edition

## 

## Overview [![Go Report Card](https://goreportcard.com/badge/gitlab.com/Zyian/xivapi-go-modular)](https://goreportcard.com/report/gitlab.com/Zyian/xivapi-go-modular)

The XIVAPI contains a **lot** of data, sometimes this data isn't applicable to all projects, so to keep the import cost low, I split the major sections of the API into seperate standalone packages.

This API is heavily influenced by CKing and their work on [xivapi-go](https://gitlab.com/paars/xiv/xivapi-go)

## Install
```bash
go get gitlab.com/Zyian/xivapi-go-modular
```

## Usage

This API excels in allowing you to cherry pick the components of the API that you wish to use. The simplest usage is:

```go
import "gitlab.com/Zyian/xivapi-go-modular/patchlist"

func main() {
    p, _ := patchlist.NewPatchList(true, "1111XXXXX0000BB")
    patches, _ := p.PatchList()

    for _, patch := range patches {
        fmt.Println(patch)
    }
}
```

The same process applies to other components, if you are using multiple components, it is usually best to use a single web client. To attach a single webclient to all components, use the following format.

```go
import (
    "gitlab.com/Zyian/xivapi-go-modular/patchlist"
    "gitlab.com/Zyian/xivapi-go-modular/character"
    "gitlab.com/Zyian/xivapi-go-modular/client"
)

func main() {
    w := client.New("1111XXXXX0000BB")
    p, _ := patchlist.NewPatchList(false, "")
    p.AttachWebClient(w)
    c, _ := character.NewCharacterClient(false, "")
    c.AttachWebClient(w)

    patches, _ := p.PatchList()

    for _, patch := range patches {
        fmt.Println(patch)
    }
}
```

If you prefer importing all of the API there's a nice All-in-One package at the root.

```go
import "gitlab.com/Zyian/xivapi-go-modular"

func main() {
    api := modular.NewApi("1111XXXXX0000BB")
    patches, _ := api.PatchList.PatchList()

    for _, patch := range patches {
        fmt.Println(patch)
    }
}
```

## Contributing

[Guidelines](CONTRIBUTING.md)

## License
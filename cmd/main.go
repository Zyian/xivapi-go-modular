package main

import (
	"fmt"
	"strconv"

	"gitlab.com/Zyian/xivapi-go-modular"
)

func main() {
	api := xivmodular.NewApi("c8e1da9ec3e6469abbbeb519")

	// pl, _ := c.PatchList()

	// r, _ := api.Character.SearchCharacter("Alex", "", 0)

	zy, err := api.Character.Character("10771250")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(zy.Character.Name, zy.Character.Server)

	fmt.Println(api.Character.RequestUpdate(strconv.FormatInt(int64(zy.Character.ID), 10)))

}

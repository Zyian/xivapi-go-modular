package character

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net/url"
	"strconv"
	"strings"

	"github.com/google/go-querystring/query"

	"gitlab.com/Zyian/xivapi-go-modular/client"
	"gitlab.com/Zyian/xivapi-go-modular/xiverrors"
)

func NewCharacterClient(defaultClient bool, key string) (*CharacterClient, error) {
	if defaultClient && key == "" {
		return nil, xiverrors.ErrClientKeyMismatch
	}

	c := new(CharacterClient)
	if defaultClient {
		c.AttachWebClient(client.New(key))
	}

	return c, nil
}

func (c *CharacterClient) AttachWebClient(root *client.XIVAPIWeb) {
	c.rootClient = root
}

func (c *CharacterClient) SearchCharacter(name, server string, page int) (*CharacterSearch, error) {
	if name == "" {
		return nil, errors.New("name not specified")
	}
	opts := CharacterSearchQuery{name, server, page}
	v, err := query.Values(opts)
	if err != nil {
		return nil, err
	}

	uri, _ := url.Parse(fmt.Sprintf("%v%v", client.BaseURL, "/character/search"))
	uri.RawQuery = v.Encode()

	r := &CharacterSearch{}
	if err := c.rootClient.RequestJSON(client.MethodGet, uri, nil, &r); err != nil {
		return nil, err
	}

	return r, nil
}

func (c CharacterSearchResult) String() string {
	return fmt.Sprintf("{ %v on %v | Avatar: %v }", c.Name, c.Server, c.Avatar)
}

func (c *CharacterClient) Character(lsi string, extraData ...string) (*CharacterInfo, error) {
	if lsi == "" {
		return nil, errors.New("lodestone id not provided")
	}

	uri, _ := url.Parse(fmt.Sprintf("%v%v%v?data=%v", client.BaseURL, "/character/", lsi, strings.Join(extraData, ",")))

	r := &CharacterInfo{}
	if err := c.rootClient.RequestJSON(client.MethodGet, uri, nil, &r); err != nil {
		return nil, err
	}

	return r, nil
}

func (c *CharacterClient) RequestUpdate(lsi string) (bool, error) {
	uri, err := url.Parse(fmt.Sprintf("%v%v%v%v", client.BaseURL, "/character/", lsi, "/update"))

	resp, err := c.rootClient.Request(client.MethodGet, uri, nil)
	if err != nil {
		return false, err
	}
	defer resp.Close()
	val, _ := ioutil.ReadAll(resp)
	return strconv.ParseBool(string(val))
}

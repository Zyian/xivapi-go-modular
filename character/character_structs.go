package character

import (
	"time"

	"gitlab.com/Zyian/xivapi-go-modular/client"
)

type CharacterClient struct {
	rootClient *client.XIVAPIWeb
}

type CharacterSearchQuery struct {
	Name   string `url:"name"`
	Server string `url:"server"`
	Page   int    `url:"page"`
}

type CharacterSearch struct {
	Pagination struct {
		Page           int
		PageNext       int
		PagePrevious   int
		PageTotal      int
		Results        int
		ResultsPerPage int
		ResultsTotal   int
	} `json:"Pagination"`
	Results []CharacterSearchResult `json:"Results"`
}

type CharacterSearchResult struct {
	Avatar       string
	FeastMatches int
	ID           string
	Name         string
	Rank         string
	RankIcon     string
	Server       string
}

type CharacterInfo struct {
	Achievements Achievements
	Character    Character
	Info         CharacterStateInfo
}

type Character struct {
	ActiveClassJob        *ClassJob
	Avatar                string
	Bio                   string
	ClassJobs             *ClassJobs
	FreeCompanyId         string
	GearSet               *GearSet
	Gender                int
	GrandCompany          *CharacterGrandCompany
	GuardianDeity         int
	ID                    int
	Name                  string
	NameDay               string
	ParseDate             string
	Portrait              string
	PvPTeam               string
	Race                  int
	Server                string
	Title                 int
	Town                  int
	Tribe                 int
	VerificationToken     string
	VerificationTokenPass bool
	// Mounts                string
	// Minions               string
}

type GearSet struct {
	Attributes Attributes
	ClassID    int
}

type Attributes struct {
	Strength            int `json:"1"`
	Dexterity           int `json:"2"`
	Vitality            int `json:"3"`
	Intelligence        int `json:"4"`
	Mind                int `json:"5"`
	CritHitRate         int `json:"27"`
	Determination       int `json:"44"`
	DirectHitRate       int `json:"22"`
	Defense             int `json:"21"`
	MagicDefense        int `json:"24"`
	AttackPower         int `json:"20"`
	SkillSpeed          int `json:"45"`
	AttackMagicPotency  int `json:"33"`
	HealingMagicPotency int `json:"34"`
	SpellSpeed          int `json:"46"`
	Tenacity            int `json:"19"`
	Piety               int `json:"6"`
	HP                  int `json:"7"`
	MP                  int `json:"8"`
	TP                  int `json:"9"`
}

type ClassJob struct {
	ClassID      int
	ExpLevel     int
	ExpLevelMax  int
	ExpLevelTogo int
	JobID        int
	Level        int
}

type ClassJobs struct {
	Armorer       *ClassJob `json:"10_10"`
	Goldsmith     *ClassJob `json:"11_11"`
	Leatherworker *ClassJob `json:"12_12"`
	Weaver        *ClassJob `json:"13_13"`
	Alchemist     *ClassJob `json:"14_14"`
	Culinarian    *ClassJob `json:"15_15"`
	Carpenter     *ClassJob `json:"8_8"`
	Blacksmith    *ClassJob `json:"9_9"`
	Miner         *ClassJob `json:"16_16"`
	Botanist      *ClassJob `json:"17_17"`
	Fisher        *ClassJob `json:"19_19"`
	Paladin       *ClassJob `json:"1_19"`
	Monk          *ClassJob `json:"2_20"`
	Warrior       *ClassJob `json:"3_21"`
	Dragoon       *ClassJob `json:"4_22"`
	Bard          *ClassJob `json:"5_23"`
	WhiteMage     *ClassJob `json:"6_24"`
	BlackMage     *ClassJob `json:"7_25"`
	Summoner      *ClassJob `json:"26_27"`
	Scholar       *ClassJob `json:"26_28"`
	Ninja         *ClassJob `json:"29_30"`
	Machinist     *ClassJob `json:"31_31"`
	DarkKnight    *ClassJob `json:"32_32"`
	Astrologian   *ClassJob `json:"33_33"`
	Samurai       *ClassJob `json:"34_34"`
	RedMage       *ClassJob `json:"35_35"`
}
type CharacterGrandCompany struct {
	NameID int
	RankID int
}

type Achievements struct {
	List         []Achievement `json:"List"`
	ParseDateRaw string        `json:"ParseDate"`
	ParseDate    time.Time
	Points       int `json:"Points"`
}

type Achievement struct {
	ID      int    `json:"ID"`
	DateRaw string `json:"Date"`
	Date    time.Time
}

type CharacterStateInfo struct {
	Achievements       State
	FreeCompany        State
	Character          State
	FreeCompanyMembers State
	Friends            State
	PvPTeam            State
}

type State struct {
	State   int
	Updated string
}

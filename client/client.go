package client

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"runtime"
	"time"
)

const (
	BaseURL string = "https://xivapi.com"

	defaultTimeout = 30 * time.Second

	Version string = "0.1.0"
)

type XIVAPIWeb struct {
	WebClient *http.Client

	PreRequest  func(*http.Request) error
	PostRequest func(*http.Response) error

	key string
}

func New(key string) *XIVAPIWeb {
	c := new(XIVAPIWeb)
	c.key = key

	c.WebClient = &http.Client{
		Timeout: defaultTimeout,
	}

	return c
}

func (c *XIVAPIWeb) Request(method Methods, uri *url.URL, body io.ReadCloser) (io.ReadCloser, error) {
	query := uri.Query()
	query.Set("key", c.key)
	uri.RawQuery = query.Encode()

	req, err := http.NewRequest(string(method), uri.String(), body)
	if err != nil {
		return nil, err
	}

	if body != nil {
		req.Body = body
	}

	if c.PreRequest != nil {
		if err := c.PreRequest(req); err != nil {
			return nil, err
		}
	}

	req.Header.Add("User-Agent", fmt.Sprintf("gitlab.com-zyian-xiv-xivmodular/%v %v (%v)", Version, runtime.GOOS, runtime.GOARCH))
	res, err := c.WebClient.Do(req)
	if err != nil {
		return nil, err
	}

	if res.StatusCode != http.StatusOK {
		return nil, NewStatusCodeError(res.StatusCode)
	}
	if c.PostRequest != nil {
		if err := c.PostRequest(res); err != nil {
			return nil, err
		}
	}

	return res.Body, nil
}

// RequestJSON calls Request to get the io.ReadCloser and then converts the body to the specified JSON object
func (c *XIVAPIWeb) RequestJSON(method Methods, uri *url.URL, body io.ReadCloser, jsonStruct interface{}) error {
	responseBody, err := c.Request(method, uri, body)
	if err != nil {
		return err
	}

	defer responseBody.Close()
	json.NewDecoder(responseBody).Decode(jsonStruct)
	return nil
}

// Methods enumerates every supported HTTP Method
type Methods string

// Collection of all supported HTTP Methods
const (
	MethodGet    Methods = "GET"
	MethodPost           = "POST"
	MethodPut            = "PUT"
	MethodDelete         = "DELETE"
)

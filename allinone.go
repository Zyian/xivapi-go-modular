package xivmodular

import (
	"gitlab.com/Zyian/xivapi-go-modular/character"
	"gitlab.com/Zyian/xivapi-go-modular/client"
	"gitlab.com/Zyian/xivapi-go-modular/patchlist"
)

type XIVAPI struct {
	rootClient *client.XIVAPIWeb
	PatchList  *patchlist.PatchListClient
	Character  *character.CharacterClient
}

func NewApi(key string) *XIVAPI {
	if key == "" {
		return nil
	}

	c := new(XIVAPI)
	c.rootClient = client.New(key)

	c.PatchList, _ = patchlist.NewPatchList(false, "")
	c.PatchList.AttachWebClient(c.rootClient)

	c.Character, _ = character.NewCharacterClient(false, "")
	c.Character.AttachWebClient(c.rootClient)

	return c
}

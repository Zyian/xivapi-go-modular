package xivmodular

import "testing"

func TestAllInOneAPIFailOnBlankKey(t *testing.T) {
	if api := NewApi(""); api != nil {
		t.Errorf("blank key should not produce api client")
	}
}
